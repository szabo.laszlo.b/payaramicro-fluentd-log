sudo docker run -d -p 9200:9200 --rm -p 9300:9300 --name elastic-host --network efk-net -e "discovery.type=single-node" -v /data/elasticsearch/data/:/usr/share/elasticsearch/data/ docker.elastic.co/elasticsearch/elasticsearch:7.6.1


sudo docker run -d --net  efk-net --name fluentd --rm -p 24224:24224 -v fluent.conf:/fluentd/etc/fluent.conf fluentd-elasticsearch:latest


sudo docker run -d --rm --network efk-net --name kibana -p 5601:5601 -e ELASTICSEARCH_HOSTS=http://elastic-host:9200 docker.elastic.co/kibana/kibana:7.6.1


